from django.contrib import admin
from django.utils.html import format_html

from tasks.models import Project, Member, Task, TaskSession, Rate


@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
    list_display = ['user', 'rate']


class MemberInline(admin.TabularInline):
    model = Member


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'owner', 'started', 'url', 'active']
    inlines = [MemberInline]


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ['user', 'project', 'joined']


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['member', 'started', 'duration']
    list_filter = ['member']
    date_hierarchy = 'started'


@admin.register(TaskSession)
class TaskSessionAdmin(admin.ModelAdmin):
    list_display = ['task', 'started', 'ended', 'duration', 'commit_link']
    list_filter = ['task__member__user']
    date_hierarchy = 'started'

    def commit_link(self, obj):
        return format_html(
            "<a href='{}'>{}</a>".format(obj.commit_url, obj.commit))
