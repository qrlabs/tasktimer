from django import forms


class TaskForm(forms.Form):
    activity = forms.CharField(
        max_length=250, widget=forms.Textarea(attrs={'rows': "4"}))


class SessionForm(forms.Form):
    hours = forms.IntegerField()
    minutes = forms.IntegerField()
    commit = forms.CharField(
        max_length=250,
        required=False,
        widget=forms.Textarea(attrs={'rows': '3'}))


class DateForm(forms.Form):
    start = forms.DateField()


class DateRangeForm(forms.Form):
    start = forms.DateField()
    end = forms.DateField()
