from django.conf.urls import url

from tasks import views


urlpatterns = [
    url(r'list/(?P<id>\d+)/$', views.activities, name='task_list'),
    url(r'hrs/(?P<id>\d+)/$', views.work_hours, name='task_work_hours'),
    url(r'summary/$', views.summary, name='task_summary'),
    url(r'days/(?P<id>\d+)/$', views.daily_hours, name='task_daily_hours'),
    url(r'reports/$', views.reports, name='task_reports'),
    url(r'sessions_json/$', views.sessions_json),
    url(r'start/(?P<id>\d+)/$', views.start_task, name='task_start'),
    url(r'continue/(?P<id>\d+)/$', views.continue_task, name='task_continue'),
    url(r'time/(?P<id>\d+)/$', views.time_task, name='task_timing'),
    url(r'sessions/(?P<id>\d+)/$', views.sessions, name='task_sessions'),
    url(r'edit_session/(?P<id>\d+)/$', views.edit_session, name='task_edit_session'),
]
