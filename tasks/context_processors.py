from tasks.models import TaskSession


def active_tasks(request):
    if not request.user.is_authenticated():
        return {
            'active_sessions': [],
            'session_count': 0,
            'logged_in': False
        }
    active_sessions = TaskSession.objects.filter(
        task__member__user=request.user, ended__isnull=True)
    session_count = active_sessions.count()
    #session_count = len([i for i in active_sessions])
    return {
        'active_sessions': active_sessions,
        'session_count': session_count,
        'logged_in': True
    }
