from collections import defaultdict

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils import timezone
from django.http import JsonResponse
from dateutil import relativedelta

from tasks.models import Task, Project, Member, TaskSession, Rate
from tasks.forms import TaskForm, SessionForm, DateForm, DateRangeForm


def summary(request):
    form = DateRangeForm(request.GET)
    if form.is_valid():
        start = form.cleaned_data['start']
        end = form.cleaned_data['end']
    else:
        start = timezone.now().date()
        end = timezone.now().date()

    sessions = TaskSession.objects.filter(
        started__date__gte=start,
        started__date__lte=end)

    d = defaultdict(int)
    for sess in sessions:
        d[sess.task.member.user.username] += sess.duration

    out = []
    for key, val in d.items():
        try:
            _rate = Rate.objects.get(user__username=key)
            rate = _rate.rate
        except Rate.DoesNotExist:
            rate = 0
        hrs = val/3600.
        amt = hrs * int(rate)
        hrs_fmt = '{:.2f}'.format(hrs)
        amt_fmt = '{:.2f}'.format(amt)
        item = [key, rate, hrs, amt, hrs_fmt, amt_fmt]
        out.append(item)
    #total_hrs = '{:.2f}'.format(sum(d.values()) / 3600.)
    total_hrs = '{:.2f}'.format(sum([i[2] for i in out]))
    total_amt = '{:.2f}'.format(sum([i[3] for i in out]))
    return render(
        request,
        'tasks/summary.html',
        {
            'tasks': out,
            'form': form,
            'total_time': total_hrs,
            'cost': total_amt
        }
    )


def daily_hours(request, id):
    project = get_object_or_404(Project, pk=id)
    sessions = TaskSession.objects.filter(task__member__project=project)

    form = DateForm(request.GET)
    if form.is_valid():
        start = form.cleaned_data['start']
        sessions = sessions.filter(started__date__gte=start)

    d = defaultdict(int)
    for sess in sessions:
        d[sess.started.strftime('%d-%b-%Y')] += sess.duration

    total_hrs = sum(d.values()) / 3600.
    cost = total_hrs * float(project.rate)
    out = [
        (k, '{:.2f}'.format(v/3600.),
            int(v*float(project.rate)/3600)) for k, v in d.items()]
    return render(
        request,
        'tasks/days.html',
        {
            'tasks': out,
            'project': project,
            'form': form,
            'total_time': total_hrs,
            'cost': cost
        }
    )


def work_hours(request, id):
    #import pdb;pdb.set_trace()
    project = get_object_or_404(Project, pk=id)
    tasks = Task.objects.filter(member__project=project)

    form = DateForm(request.GET)

    if form.is_valid():
        start = form.cleaned_data['start']
        tasks = tasks.filter(started__date__gte=start)

    total_time = sum(tsk.duration for tsk in tasks)//(60 * 60)
    cost = int(total_time) * project.rate
    return render(
        request,
        'tasks/hours.html',
        {
            'tasks': tasks,
            'project': project,
            'form': form,
            'total_time': total_time,
            'cost': cost
        }
    )


@login_required
def activities(request, id):
    project = get_object_or_404(Project, pk=id)
    tasks = Task.objects.filter(
        member__user=request.user, member__project=project).order_by('-id')
    return render(
        request, 'tasks/tasks.html', {'tasks': tasks, 'project': project})


@login_required
def start_task(request, id):
    project = get_object_or_404(Project, pk=id)
    member = Member.objects.get(user=request.user, project=project)
    if request.method == 'POST':
        TaskSession.objects.filter(
            task__member__user=request.user,
            ended__isnull=True).update(
            ended=timezone.now())
        form = TaskForm(request.POST)
        if form.is_valid():
            activity = form.cleaned_data['activity']
            task = Task.objects.create(
                member=member, started=timezone.now(), activity=activity)
            #make sure no other task is running
            #member.stop_running_tasks()

            task_session = TaskSession.objects.create(task=task)
            return redirect('task_timing', id=task_session.id)
    else:
        form = TaskForm()
    return render(
        request, 'tasks/new_task.html', {'form': form, 'project': project})


@login_required
def continue_task(request, id):
    task = get_object_or_404(Task, pk=id)
    task_session = TaskSession.objects.create(task=task)
    return redirect('task_timing', id=task_session.id)


@login_required
def time_task(request, id):
    task_session = get_object_or_404(TaskSession, pk=id)
    session_time = (timezone.now() - task_session.started).total_seconds()
    activity_time = task_session.task.duration
    status = request.GET.get('activity_status', '')
    if status == 'pause':
        task_session.ended = timezone.now()
        task_session.save()
    elif status == 'play':
        new_session = TaskSession.objects.create(task=task_session.task)
        return redirect('task_timing', id=new_session.id)
    elif status == 'stop':
        task_session.ended = timezone.now()
        task_session.save()
        return redirect('task_list', id=task_session.task.member.project.id)
    #import pdb;pdb.set_trace()
    #if (timezone.now() - task_session.started).total_seconds > 2:
        #task_session.ended = timezone.now()
        #new_session = TaskSession.objects.create(task=task_session.task)
        #return redirect('task_timing', id=new_session.id)
    return render(
        request,
        'tasks/counting.html',
        {
            'time_spent': session_time + activity_time,
            'task_session': task_session,
            'status': status or 'play',
        }
    )


def sessions(request, id):
    task = get_object_or_404(Task, pk=id)
    return render(
        request,
        'tasks/sessions.html',
        {
            'task_sessions': task.sessions.all(),
            'project_id': task.member.project.id,
            'task': task
        }
    )


def edit_session(request, id):
    session = get_object_or_404(TaskSession, pk=id)
    if not session.ended:
        session.ended = timezone.now()
        session.save()
    total_secs = (session.ended - session.started).total_seconds()
    init_hrs = int(total_secs)/3600
    init_mins = (int(total_secs) % 3600) / 60
    initial_data = {'hours': init_hrs, 'minutes': init_mins}
    if request.method == 'POST':
        form = SessionForm(request.POST, initial=initial_data)
        if form.is_valid():
            hours = form.cleaned_data['hours']
            minutes = form.cleaned_data['minutes']
            timediff = timezone.timedelta(hours=hours, minutes=minutes)
            session.ended = session.started + timediff
            session.commit = form.cleaned_data['commit']
            session.save()
            return redirect('task_sessions', id=session.task.id)
    else:
        form = SessionForm(initial=initial_data)
    return render(
        request,
        'tasks/edit_session.html',
        {
            'form': form,
            'task_session': session
        }
    )


def reports(request):
    return render(request, 'tasks/reports.html', {})


def sessions_json(request):
    date_range = request.GET.get('daterange', 'current')

    users = User.objects.filter(is_active=True)
    #users = defaultdict(list)
    today = timezone.now().date()
    last_month = today + relativedelta.relativedelta(months=-1)
    this_month_start = today + relativedelta.relativedelta(day=1)
    last_month_start = last_month + relativedelta.relativedelta(day=1)
    last_month_end = this_month_start + relativedelta.relativedelta(days=-1)
    if date_range == 'current':
        start = this_month_start
        end = today
    else:
        start = last_month_start
        end = last_month_end
    dates = [start + timezone.timedelta(i) for i in range((end-start).days+1)]
    labels = [dt.strftime('%d %b') for dt in dates]
    print dates

    data = {}
    summary = []
    for usr in users:
        usr_data = []
        sess_dict = defaultdict(int)
        for dt in dates:
            sess_dict[dt] = 0

        for usr_session in TaskSession.objects.filter(
                started__date__gte=start, task__member__user=usr):
            sess_dict[usr_session.started.date()] += usr_session.duration/3600.

        for dt in dates:
            usr_data.append(sess_dict[dt])
        data[usr.username] = usr_data
        summary.append({'name': usr.username, 'time': sum(sess_dict.values())})
    print data
    return JsonResponse({'data': data, 'dates': labels, 'summary': summary})
