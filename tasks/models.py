from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Rate(models.Model):
    user = models.ForeignKey(User)
    rate = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __unicode__(self):
        return self.user.username


class Project(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User)
    started = models.DateTimeField(default=timezone.now)
    url = models.URLField(blank=True, null=True)
    rate = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['-started']

    def __unicode__(self):
        return self.name

    @property
    def member_count(self):
        return self.project_members.count()

    @property
    def task_count(self):
        return Task.objects.filter(member__project=self).count()


class Member(models.Model):
    project = models.ForeignKey(Project, related_name='project_members')
    user = models.ForeignKey(User)
    joined = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('project', 'user')

    def __unicode__(self):
        return self.user.username

    def stop_running_tasks(self):
        TaskSession.objects.filter(
            task__member=self,
            ended__isnull=True
        ).update(ended=timezone.now())

    @property
    def running_tasks(self):
        TaskSession.objects.filter(
            task_member=self, task_ended__isnull=True).count()

    @property
    def total_hours(self):
        start = timezone.now().replace(day=1, hour=0, minute=0, second=0)
        total_seconds = sum([i.duration for i in TaskSession.objects.filter(
            task__member=self, started__gte=start)])
        return total_seconds / 3600.


class Task(models.Model):
    member = models.ForeignKey(Member)
    started = models.DateTimeField()
    activity = models.TextField(null=True, blank=True)
    billable = models.BooleanField(default=True)

    def __unicode__(self):
        return unicode(self.activity)

    @property
    def duration_human(self):
        seconds = self.duration
        hrs = int(seconds)/3600
        mins = (int(seconds) % 3600) / 60
        return '%s hrs %s mins' % (hrs, mins)

    @property
    def duration(self):
        return sum(t.duration for t in self.sessions.all())

    @property
    def session_count(self):
        return self.sessions.count()


class TaskSession(models.Model):
    task = models.ForeignKey(Task, related_name='sessions')
    started = models.DateTimeField(default=timezone.now)
    ended = models.DateTimeField(blank=True, null=True)
    commit = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return unicode(self.task)

    @property
    def duration(self):
        if self.ended:
            return (self.ended - self.started).total_seconds()
        else:
            return 0

    @property
    def duration_human(self):
        seconds = self.duration
        hrs = int(seconds)/3600
        mins = (int(seconds) % 3600) / 60
        return '%s hrs %s mins' % (hrs, mins)

    @property
    def commit_url(self):
        return '{}{}'.format(self.task.member.project.url, self.commit)
