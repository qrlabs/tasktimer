# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2018-11-02 15:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_project_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='rate',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
