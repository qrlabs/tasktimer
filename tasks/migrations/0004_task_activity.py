# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-10-20 20:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_auto_20161020_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='activity',
            field=models.TextField(blank=True, null=True),
        ),
    ]
