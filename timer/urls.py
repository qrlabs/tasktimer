"""timer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

#from tasks import views as task_views
from core import views as core_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/$', core_views.dash, name='profile'),
    url(r'^accounts/logout/$',
        auth_views.logout,
        {'next_page': '/accounts/login/'},
        name='logout'),
    url(r'^accounts/login/$',
        auth_views.login,
        {'template_name': 'core/pages-login.html'},
        name='login'),
    url(r'^register/$', core_views.registration, name='registration'),
    url(r'^tasks/', include('tasks.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^$', RedirectView.as_view(url=reverse_lazy('profile'))),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
