from django.conf.urls import url

from api import views


urlpatterns = [
    url(r'^projects/$', views.get_projects),
    url(r'^session/$', views.get_last_session),
    url(r'^start/(?P<id>\d+)/$', views.start_task),
    url(r'^create/$', views.create_task),
    url(r'^update/(?P<id>\d+)/$', views.update_task),
    url(r'^stop/(?P<id>\d+)/$', views.stop_task),
    url(r'^login/$', views.login),
]
