from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.utils import timezone
#from django.contrib.auth import login, authenticate
#from django.contrib.auth.models import User
from core.forms import LoginForm

from tasks.models import Project, Task, TaskSession, Member


def get_projects(request):
    username = request.GET.get('user', None)
    if not username:
        return JsonResponse({'projects': []})
    projects = Project.objects.filter(
        project_members__user__username=username, active=True)
    out = [
        {
            'id': project.id,
            'name': project.name
        } for project in projects]
    return JsonResponse({'projects': out})


def get_last_session(request):
    username = request.GET.get('user', None)
    if not username:
        return JsonResponse({'success': False})
    _sessions = TaskSession.objects.filter(
        task__member__user__username=username).order_by('-id')
    if _sessions:
        _session = _sessions[0]
        return JsonResponse({
            'session': _session.id,
            'task_id': _session.task.id,
            'task': _session.task.activity,
            'project': _session.task.member.project.name,
            'started': _session.started.strftime('%Y-%m-%dT%H%M%S'),
            'ended': True if _session.ended else False,
            'success': True,
            'duration': _session.task.duration
        })
    else:
        return JsonResponse({'success': False})


def update_task(request, id):
    sess = TaskSession.objects.get(pk=id)
    sess.ended = timezone.now()
    sess.save()
    total = sum(i.duration for i in TaskSession.objects.filter(
        started__date=timezone.now().date(),
        task__member=sess.task.member))
    return JsonResponse({'success': True, 'total': total})


def create_task(request):
    user_id = request.GET.get('userId')
    project_id = request.GET.get('projectId')
    activity = request.GET.get('activity')
    try:
        member = Member.objects.get(
            user__id=user_id,
            project__id=project_id)
    except Member.DoesNotExist:
        return HttpResponseBadRequest('Wrong user info')
    else:
        started = timezone.now()
        task = Task.objects.create(
            member=member,
            activity=activity,
            started=started)
        sess = TaskSession.objects.create(
            task=task,
            started=started,
            ended=started)
        return JsonResponse({'currentSession': sess.id})


def start_task(request, id):
    task = get_object_or_404(Task, pk=id)
    TaskSession.objects.create(task=task)
    return JsonResponse({'success': True})


def stop_task(request, id):
    task_session = get_object_or_404(TaskSession, pk=id)
    task_session.ended = timezone.now()
    task_session.save()
    return JsonResponse({
        'session': task_session.id,
        'task_id': task_session.task.id,
        'task': task_session.task.activity,
        'project': task_session.task.member.project.name,
        'started': task_session.started.strftime('%Y-%m-%dT%H%M%S'),
        'ended': True if task_session.ended else False,
        'success': True,
        'duration': task_session.task.duration
    })


def login(request):
    if request.method == 'GET':
        form = LoginForm(request.GET)
        if form.is_valid():
            user = form.cleaned_data['user']
            username = user.username
            projects = Project.objects.filter(
                project_members__user__username=username, active=True)
            _projects = [
                {
                    'id': project.id,
                    'name': project.name
                } for project in projects]
            return JsonResponse({
                'projects': _projects,
                'userId': user.id,
                'userName': user.username
            })
    return HttpResponseBadRequest('Wrong credentials')
