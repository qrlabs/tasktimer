from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from core.forms import LoginForm, RegistrationForm
from tasks.models import Project, Task, TaskSession, Member


def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            login_user(request, name, email, password)
            return redirect('profile')
    else:
        form = RegistrationForm()
    return render(request, 'core/registration.html', {'form': form})


def login_user(request, name, email, password):
    try:
        user = User.objects.get(username=email)
        user.set_password(password)
        user.save()
    except User.DoesNotExist:
        user = User.objects.create_user(email, email=email, password=password)
        user.first_name = name
        user.save()
    usr = authenticate(username=email, password=password)
    login(request, usr)


@login_required
def dash(request):
    projects = Project.objects.filter(project_members__user=request.user)
    task_count = Task.objects.filter(member__user=request.user).count()

    start = timezone.now().replace(day=1, hour=0, minute=0, second=0)
    total_seconds = sum([sess.duration for sess in TaskSession.objects.filter(
        task__member__user=request.user, started__gte=start)])
    return render(
        request,
        'core/dash.html',
        {
            'projects': projects,
            'task_count': task_count,
            'total_hours': total_seconds / 3600.
        })
